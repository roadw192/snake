var lives = 1; // Anzahl der Leben
var points = 0; // Anzahl der Punkte
var x = 2 ; // Variable f�r Steuerung
var y = 0 ; // Variable f�r l�ngere werden der Schlange ( y wird erh�ht um das n�chste Objekt in der Liste zu erreichen )
var t1; // Globale Variable f�r den Timer um das Zeitintervall stoppen zu k�nnen 
var start_size = 6;
var poison_delete_timer = null;

var food = null; // list of foods (eigentlich ein Array)
var poison = null; // list of gift (eigentlich ein Array)
var snake = null; // list of snake (eigentlich ein Array)

var grow = false;


function r1_intersects_r2(r1,r2) // Funktion zum �berpr�fen ob sie 2 Objekte �berschneiden
{
	if (r1.x + r1.w < r2.x)
		return false; // r2 rechts von r1 ==> keine �berschneidung
		
	if (r2.x + r2.w < r1.x)
		return false; // r2 links von r1 
	
	if (r1.y + r1.h < r2.y)
		return false; // r1 �ber r2
		
	if (r2.y + r2.h < r1.y)
		return false; // r2 �ber r1

	return true;		
}

function my_snake(x,y) // Konstruktur zum Objekt "snake"
{
	// this ==> permanente Daten zum Objekt
	this.x = x;
	this.y = y;
	this.w = 10;
	this.h = 10;
	this.color = "#800";
}

function my_snake_clone(item) // Konstruktur zum Objekt "snake"
{
	// this ==> permanente Daten zum Objekt
	this.x = item.x;
	this.y = item.y;
	this.w = item.w;
	this.h = item.h;
	this.color = item.color;
}


function my_food(x,y) // Konstruktur zum Objekt "food"
{
	// this ==> permanente Daten zum Objekt
	this.x = x;
	this.y = y;
	this.w = 10;
	this.h = 10;
	this.color = "#00f";
}

function gift(x,y,w,h) // Konstruktur zum Objekt "gift"
{
	// this ==> permanente Daten zum Objekt
	this.x = x;
	this.y = y;
	this.w = w;
	this.h = h;
	this.color = "#0f0"; 
}

function load()
{
	// Timer aufziehen
	t1 = window.setInterval(f_timer, 80);

	// Eventhandler f�r KeyPress registrieren
	canvas = document.getElementById('myCanvas');
	canvas.setAttribute('tabindex','0');
	canvas.focus();
	canvas.addEventListener('keypress', f_keypress	, false);

	/* Liste f�r snake erzeugen */
    snake = new Array(); // Array f�r snake
	food = new my_food(canvas.width/2-100, canvas.height/2 -100);


	start_pos_x = canvas.width/2-10;
	start_pos_y = canvas.height/2 -10;
	for (var i=1; i <= start_size; i++)
	{
		var x = start_pos_x - (12 * i);
		var y = start_pos_y;
		snake.push(new my_snake(x, y));
	}

	f_draw();	
}

function f_draw()
 {
	lives_display = document.getElementById('lives');
	lives_display.innerHTML = lives;
	
	points_display = document.getElementById('points');
	points_display.innerHTML = points;
	

	canvas = document.getElementById('myCanvas');
	canvas.width = canvas.width; // alles neu zeichnen

	var gc = canvas.getContext("2d"); // Toolbox zum Zeichnen
	
	img = document.getElementById('background'); // Hintergrund im canvas zeichnen
	gc.drawImage(img,canvas.width - canvas.width, canvas.height - canvas.height, canvas.width, canvas.height);

	
	// snake zeichnen	
	for(var i=0; i< snake.length; i++ ) 
	{
		var s = snake[i] ;
		gc.fillStyle = s.color;
		gc.fillRect(s.x,s.y,s.w,s.h);
	}
	
	// food zeichnen	
	gc.fillStyle = food.color;
	gc.fillRect(food.x,food.y,food.w,food.h);

	// gifts zeichnen
	if (poison) 
	{
		var img = document.getElementById('gift'); // Bild f�r gift zeichnen
		gc.drawImage(img, poison.x, poison.y, poison.w, poison.h);
	}
}

function f_keypress(e)
 {	
	if (e.keyCode == 97 ) // snake nach likns (A)
	{
		x = 1 ;
	}
	if (e.keyCode == 100 )  // snake nach rechts (D)
	{
		x = 2
	}
	if (e.keyCode == 119 ) // snake nach oben (W)
	{
		x = 3
	}
	if (e.keyCode == 115 )  // snake nach unten (S)
	{
		x = 4
	}
	
}

Array.prototype.remove = function (item)
{
	var deleted;
	var index = 0;
	while ((index = this.indexOf(item, index)) != -1) {
		deleted = this.splice(index, 1);
	}
	if (deleted) {
		return deleted[0];
	}
};




var cnt=0;
function f_timer() 
{
	cnt ++;
	
	// Timer f�r gift objecte
	if (cnt % 150 == 0)
	{
		// zuf�llige Positionen f�r das gift erzeugen
		var c = Math.random(); // Zufallszahl [0.0 ... 1.0]
		var d = Math.random(); // Zufallszahl [0.0 ... 1.0]
		var y1_pos = Math.floor(c*500); // in ganze Zahl umwandeln
		var x1_pos = Math.floor(d*800); // in ganze Zahl umwandeln
		poison = new gift(x1_pos,y1_pos,10,10);// zuf�llige x-Pos und y-Pos �ber die ganze Breite
		
		// Gift nach 10s l�schen
		poison_delete_timer = setTimeout(function() { poison = null; }, 5000);		
	
		console.log('this is happening once in a second');	
	}
	
	// snake automatisch bewegen
	var last_clone = new my_snake_clone(snake[snake.length-1]);

	for (var i=snake.length-1; i > 0; i--) {
		snake[i] = new my_snake_clone(snake[i-1]);
	}
	movePoint(snake[0], x);

	// detect collisions
	
	// 1) snakes - foods
	if (food && r1_intersects_r2(food,snake[0]))
	{	
		food = null;
		
		// Nachdem food gefressen wurde neues erzeugen und zuf�llige Position erzeugen
		var a = Math.random(); // Zufallszahl [0.0 ... 1.0]
		var b = Math.random(); // Zufallszahl [0.0 ... 1.0]
		var y_pos = Math.floor(a*450); // in ganze Zahl umwandeln
		var x_pos = Math.floor(b*750); // in ganze Zahl umwandeln
		food = new my_food(x_pos,y_pos,10,10);// zuf�llige x-Pos und y-Pos �ber die ganze Breite
		
		// snakesound bei Punktegewinn
		playSound("snake");
		points += 1;
		
		console.log("grow");
		snake.push(last_clone);
		console.log("new snake-size: " + snake.length);
	}
	
	

	
	// 2) snakes - gift
	if (poison && r1_intersects_r2(poison, snake[0]))
	{
		poison = null// bei intersection das gift - objekt aus der Liste l�schen
		// ohno sound wenn man ein ein gift frisst  
		playSound("ohno");
		lives -= 1;
		clearTimeout(poison_delete_timer);
	}
	
	
	var canvas = document.getElementById('myCanvas');
	if ( lives==0 || snake[0].x + snake[0].w  > canvas.width - 10   || snake[0].x < (canvas.width-canvas.width) || snake[0].y + snake[0].h  > (canvas.height-15) 
			|| snake[0].y < (canvas.height-canvas.height)) 
	{
		var gc = canvas.getContext("2d"); // Toolbox zum Zeichnen
		gc.beginPath();
		gc.clearRect(0,0,800,500) ;
		gc.closePath();
		
		clearInterval(t1);
		t1=null;	
		playSound("GameOver");
		
		//Funktioniert derzeit nicht 
		img = document.getElementById('explosion');
		gc.drawImage(img,snake[0].x,snake[0].y);
	}	
	else
	{
		f_draw();
	}
}

function movePoint(point, direction) 
{
	if ( direction == 1 )   // snake nach likns
	{
		point.x = point.x - 12;
	}
	if ( direction == 2 )   // snake nach rechts
	{
		point.x = point.x + 12;
	}
	if ( direction == 3 )   // snake nach oben
	{
		point.y = point.y - 12;
	}
	if ( direction == 4 )   // snake nach unten
	{
		point.y = point.y + 12;
	}
}

function playSound(vieh)  // Funktion zum abspielen eines sounds
{
   var audio = document.getElementById(vieh);
   audio.play();
}